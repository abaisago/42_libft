/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_llitoa.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 11:02:17 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:27:40 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <limits.h>
#include <stdint.h>

/*
** Returns an allocated representation string of a
** long long signed integer in a specified base of up to 36.
*/

char			*ft_llitoa(long long n)
{
	size_t		i;
	char		*res;

	if (n == LLONG_MIN)
		return (ft_strdup("-9223372036854775808"));
	if ((res = (char*)ft_strnew(ft_count_digits(n, 10))) == NULL)
		return (NULL);
	i = 0;
	if (ft_llabs_is(&n))
	{
		res[0] = '-';
		i = 1;
	}
	ft_getnbr_base(n, 10, res, &i);
	return (res);
}
