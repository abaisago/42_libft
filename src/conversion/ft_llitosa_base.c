/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_llitosa_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 15:39:51 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:36:54 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <limits.h>

char	*ft_llitosa_base(long long value, int8_t base, char *tab)
{
	size_t		i;

	if (value == LLONG_MIN && base == 10)
		ft_strcpy(tab, "-9223372036854775808");
	i = 0;
	if (ft_llabs_is(&value))
	{
		tab[0] = '-';
		++i;
	}
	ft_getnbr_base(value, base, tab, &i);
	tab[i] = '\0';
	return (tab);
}
