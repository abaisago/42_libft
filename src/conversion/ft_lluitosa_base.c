/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lluitosa_base.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 15:39:51 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:36:05 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_lluitosa_base(unsigned long long value, int8_t base, char *tab)
{
	size_t		i;

	i = 0;
	ft_getnbr_base(value, base, tab, &i);
	tab[i] = '\0';
	return (tab);
}
