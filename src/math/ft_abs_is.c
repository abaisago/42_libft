/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs_is.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 06:54:22 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:23:29 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <limits.h>

/*
** Changes an integer to it's absolute value.
** Returns 1 if it has changed, 0 otherwise.
*/

int		ft_abs_is(int *n)
{
	if (*n == INT_MIN)
	{
		*n = INT_MAX;
		return (1);
	}
	if (*n < 0)
	{
		*n = -(*n);
		return (1);
	}
	return (0);
}
