/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_llabs_is.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/07 19:18:20 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:23:07 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <limits.h>

/*
** Changes a long long integer to it's absolute value.
** Returns 1 if it has changed, 0 otherwise.
*/

int		ft_llabs_is(long long *n)
{
	if (*n == LLONG_MIN)
	{
		*n = LLONG_MAX;
		return (1);
	}
	if (*n < 0)
	{
		*n = -(*n);
		return (1);
	}
	return (0);
}
