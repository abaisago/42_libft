/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_imaxabs.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/17 23:32:18 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:12:41 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <limits.h>
#include <stdint.h>

intmax_t	ft_imaxabs(intmax_t n)
{
	if (n == INTMAX_MIN)
		n = INTMAX_MAX;
	return (n < 0 ?  -n : n);
}
