/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_llabs.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/07 19:11:24 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:14:43 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include <limits.h>

long long	ft_llabs(long long n)
{
	if (n == LLONG_MIN)
		n = LLONG_MAX;
	return (n < 0 ? -n : n);
}
