/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abaisago <adam_bai@tuta.io>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 06:58:38 by abaisago          #+#    #+#             */
/*   Updated: 2021/01/07 19:51:06 by abaisago         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	void	cycle(long long nbr)
{
	if (nbr >= 10)
	{
		cycle(nbr / 10);
		cycle(nbr % 10);
	}
	else
		ft_putchar('0' + nbr);
}

void			ft_putnbr(int n)
{
	long long	nbr;

	nbr = n;
	if (ft_llabs_is(&nbr))
		ft_putchar('-');
	cycle(nbr);
}
